var mongoose = require('mongoose');
require('../models/location');
var Loc = mongoose.model('Location');



var getReviewById = function (arr, id) {
    var review = arr.filter(function(arr,id) {
        console.log(arr.id);
        return arr.id===id;
    });
    return review;
};

var sendJsonResponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.reviewsReadOne = function(req, res) {
    if (req.params && req.params.locationid && req.params.reviewid) {
        Loc
            .findById(req.params.locationid)
            .select('name reviews')
            .exec(function(err, location) {
                if (!location) {
                    sendJsonResponse(res, 404, {
                        "message": "locationID not found"
                    });
                    return;
                } else if (err) {
                    sendJsonResponse(res, 404, err);
                    return;
                }
                var response, review;
                if (location.reviews && location.reviews.length > 0) {
                    console.log(location.reviews);
                    review = getReviewById(location.reviews,req.params.reviewid);
                    console.log(review);
                    if (!review) {
                        sendJsonResponse(res, 404, {
                            "message": "reviewID not found"
                        });
                    } else {
                        response = {
                            location: {
                                name: location.name,
                                id: req.params.locationid,
                            },
                            review: review
                        };
                        sendJsonResponse(res, 200, response);
                    }
                }
            });
    } else {
        sendJsonResponse(res, 404, {
            "message": "No locationID or reviewID in request"
        });
    }
};
