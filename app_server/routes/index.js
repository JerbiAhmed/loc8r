var express = require('express');
var router = express.Router();
var locationsController = require('../controllers/locations');
var othersController = require('../controllers/others');

router.get('/',locationsController.list);
router.get('/location',locationsController.location);
router.get('/location/review/new',locationsController.review);
router.get('/about',othersController.about);

module.exports = router;
